#!/bin/bash

set -eux

chown -R www-data:www-data /var/www/html/images /var/www/html/cache /var/external_includes

cp /etc/hosts /etc/hosts2
cat /etc/hosts2 | sed "s/localhost/localhost $NONSA_DOMAINS/" > /etc/hosts

if [ "$NONSA_MODE" = "server" ]; then
  php-fpm
  nginx -g 'daemon off;'
elif [ "$NONSA_MODE" = "jobrunner" ]; then
  runuser -u www-data -m -- /opt/jobrunner/redisJobChronService --config-file=/var/www/html/config/jobrunner.json &
  runuser -u www-data -m -- /opt/jobrunner/redisJobRunnerService --config-file=/var/www/html/config/jobrunner.json
else
  # shellcheck disable=SC2016
  echo 'Unrecognized mode of operation. Set $NONSA_MODE to either "server" or "jobrunner".'
fi
