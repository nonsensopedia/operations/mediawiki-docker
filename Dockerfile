FROM php:7.4-fpm

# System dependencies
RUN set -eux; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        nano \
        screen \
        nginx \
        # For audio files
        ffmpeg \
        # Wikidiff2 needs it... for some reason.
        libthai-dev \
        git \
        librsvg2-bin \
        imagemagick \
        # For Timeline
        fonts-freefont-ttf \
        ploticus \
        # Required for SyntaxHighlighting
        python3 \
    ; \
    rm -rf /var/lib/apt/lists/*

# Compile the wikidiff2 PHP extension
WORKDIR /build
RUN set -eux; \
    git clone https://gerrit.wikimedia.org/r/mediawiki/php/wikidiff2.git ; \
    cd wikidiff2 ; \
    phpize ; \
    ./configure ; \
    make ; \
    make install

# Install the PHP extensions we need
RUN set -eux; \
    \
    savedAptMark="$(apt-mark showmanual)"; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libicu-dev \
        libonig-dev \
    ; \
    \
    docker-php-ext-install -j "$(nproc)" \
        exif \
        intl \
        mbstring \
        mysqli \
        opcache \
        pcntl \
        pdo_mysql \
    ; \
    \
    pecl install APCu-5.1.20 redis-5.3.4; \
    docker-php-ext-enable \
        apcu \
        redis \
        wikidiff2 \
    ; \
    rm -r /tmp/pear; \
    \
    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
        | awk '/=>/ { print $3 }' \
        | sort -u \
        | xargs -r dpkg-query -S \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*


# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=60'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

WORKDIR /opt/jobrunner

RUN set -eux; \
    git clone https://github.com/wikimedia/mediawiki-services-jobrunner .

WORKDIR /var/www/html

# Version
ENV MEDIAWIKI_VERSION 1.36.1

# MediaWiki setup
RUN set -eux; \
    curl -L https://gitlab.com/api/v4/projects/nonsensopedia%2Foperations%2Fmediawiki/packages/generic/mediawiki/${MEDIAWIKI_VERSION}/mediawiki-${MEDIAWIKI_VERSION}.tar.gz -o mediawiki.tar.gz; \
    tar -x -f mediawiki.tar.gz; \
    rm mediawiki.tar.gz; \
    chown -R www-data:www-data extensions skins; \
    # set the sticky bit on /var/log to allow other users do their logging thing
    chmod 1777 /var/log

COPY entrypoint.sh /entrypoint.sh
COPY zzz-docker.conf /usr/local/etc/php-fpm.d/
COPY zz-nonsa-php.ini /usr/local/etc/php/conf.d/

RUN chmod +x /usr/local/bin/*

CMD ["/entrypoint.sh"]
