# mediawiki-docker

Docker image for Nonsensopedia's MediaWiki deployment

Will it be useful to you? Nah, probably not. But you can always have a peek at how it works.

## How to make a new release
1. Update all in-house extensions and extension forks as appropriate.
1. Go to [nonsensopedia/operations/skins](https://gitlab.com/nonsensopedia/operations/skins).
    - Branch off the most recent version naming it like `nonsa/<MW version>`, for example `nonsa/1.33.7`.
    - Edit the `.gitmodules` file and set proper remotes and branches for all skins.
    - Run `./update-modules.sh`. Commit and push your changes.
1. Repeat the procedure for [nonsensopedia/operations/extensions](https://gitlab.com/nonsensopedia/operations/extensions).
1. Go to [nonsensopedia/operations/mediawiki](https://gitlab.com/nonsensopedia/operations/mediawiki).
    - Branch off the most recent version naming it like `nonsa/<MW version>`, for example `nonsa/1.33.7`.
    - Rebase the new branch on top of the new release.
    - Commit and push. This should trigger a build, make sure it passes.
1. In this repository:
   - Branch off the most recent version naming it like `<MW version>`, for example `1.33.7`.
   - Branch off that and make a merge request.
   - Set `MEDIAWIKI_VERSION` in the Dockerfile to the appropriate version.
   - Update other dependencies in the Dockerfile as appropriate.
   - Commit and push.
   - When the build passes, merge the MR.
1. Pull the image: `registry.gitlab.com/nonsensopedia/operations/mediawiki-docker:<MW version>`

## Required mounts
- `/var/www/html/LocalSettings.php`
- `/var/www/html/cache`
- `/var/www/html/images`

## Exposed ports
- 80 and 443 – nginx

## Environment variables
- `NONSA_MODE` – either `server` or `jobrunner`
- `NONSA_DOMAINS` – space-separated list of domains that should resolve to localhost
